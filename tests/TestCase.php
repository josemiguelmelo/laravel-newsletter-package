<?php
namespace JoseMiguelMelo\Newsletter\Tests;

use Orchestra\Testbench\TestCase as BaseTestCase;
use JoseMiguelMelo\Newsletter\NewsletterServiceProvider;

class TestCase extends BaseTestCase
{
    protected function getEnvironmentSetUp($app)
    {
        // Setup default database to use sqlite :memory:
        $app['config']->set('database.default', 'testbench');
        $app['config']->set('database.connections.testbench', [
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => '',
        ]);

        $app['config']->set('newsletters', [
            'from_email'      => env('NEWSLETTER_EMAIL', 'newsletters@email.com'),
            'from_name'       => env('NEWSLETTER_NAME', 'Newsletters'),
            'subject'         => env('NEWSLETTER_SUBJECT', 'Newsletters subject'),
            'newsletter_view' => env('NEWSLETTER_VIEW', 'newsletter::mails.default'),
            'include_routes'  => env('NEWSLETTER_ROUTES', true),
        ]);

        if (file_exists(__DIR__ . '/../.env.test')) {
            (new \Dotenv\Dotenv(__DIR__ . '/../', '.env.test'))->load();
        }
    }

    public function setUp()
    {
        parent::setUp();
        $this->artisan('newsletter:migrate');
        $this->withFactories(__DIR__ . '/../database/factories');
    }

    public function getPackageProviders($app)
    {
        return [NewsletterServiceProvider::class];
    }

    public function testOne()
    {
        $this->assertTrue(true);
    }
}
<?php
namespace JoseMiguelMelo\Newsletter\Tests;

use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use JoseMiguelMelo\Newsletter\Models\Newsletter;


class NewsletterTest extends TestCase
{

    protected $newsletter;

    public function setUp()
    {
        parent::setUp(); // Don't forget this!

        $this->newsletter = factory(Newsletter::class)->make();
        $this->newsletter->active = true;
        $this->newsletter->save();
    }

    /**
     * Test if newsletter factory worked and save the newsletter subscription to the database.
     */
    public function testNewsletterFactory()
    {
        $this->newsletter->save();

        $newsletterAdded = Newsletter::find($this->newsletter->id);
        $this->assertNotNull($newsletterAdded);
    }

    /**
     * Test Newsletter frequency mutator.
     */
    public function testNewsletterMutator(){

        // Test if set Mutator worked
        $this->newsletter->frequency = 'this_should_give_daily';
        self::assertEquals(0, $this->newsletter->getAttributes()['frequency']);

        $this->newsletter->frequency = 'monthly';
        self::assertEquals(2, $this->newsletter->getAttributes()['frequency']);

        $this->newsletter->frequency = 'weekly';
        self::assertEquals(1, $this->newsletter->getAttributes()['frequency']);

        $this->newsletter->frequency = 'daily';
        self::assertEquals(0, $this->newsletter->getAttributes()['frequency']);

        // Test if get Mutator worked
        $this->newsletter->frequency = 'this_should_give_daily';
        self::assertEquals('daily', $this->newsletter->frequency);

        $this->newsletter->frequency = 'monthly';
        self::assertEquals('monthly', $this->newsletter->frequency);

        $this->newsletter->frequency = 'weekly';
        self::assertEquals('weekly', $this->newsletter->frequency);

        $this->newsletter->frequency = 'daily';
        self::assertEquals('daily', $this->newsletter->frequency);
    }

    /**
     * According to the newsletter frequency, test if it should send today or not.
     */
    public function testNewsletterMustSendChecker()
    {
        $today = Carbon::now();

        /*** Test for daily frequency ****/

        // Last sent today. Must return false.
        $this->newsletter->last_sent = $today;
        self::assertFalse($this->newsletter->mustSend());


        // Last sent 2 days ago. Must return true
        $this->newsletter->last_sent = $today->subDay(2);
        self::assertTrue($this->newsletter->mustSend());
        $this->newsletter->last_sent = $today->addDay(2);


        /*** Test for monthly frequency ****/


        $this->newsletter->frequency = 'monthly';

        // Last sent 2 days ago. Must return false
        $this->newsletter->last_sent = $today->subDay(2);
        self::assertFalse($this->newsletter->mustSend());
        $today->addDay(2);

        // Last sent 32 days ago. Must return true
        $this->newsletter->last_sent = $today->subDay(32);
        self::assertTrue($this->newsletter->mustSend());
        $today->addDay(32);


        /**** Test for weekly frequency ***/

        $this->newsletter->frequency = 'weekly';

        // Last sent 2 days ago. Must return false.
        $this->newsletter->last_sent = $today->subDay(2);
        self::assertFalse($this->newsletter->mustSend());
        $today->addDay(2);

        // Last sent 8 days ago. Must return true.
        $this->newsletter->last_sent = $today->subDay(8);
        self::assertTrue($this->newsletter->mustSend());
        $today->addDay(8);
    }

    /**
     * Test NewsletterController@ store method handler
     */
    public function testStoreRequest()
    {
        /** Test successful POST */
        $this->json('POST', '/newsletter/store', ['email' => 'testingemail@mail.com'])
            ->seeJson(
                [
                    'error' => false,
                ]);

        /** Test unsuccessful POST */
        $this->json('POST', '/newsletter/store', ['mail' => 'testingemail@mail.com'])
            ->seeJson(
                [
                    'error' => true,
                ]);

    }

    /**
     * Test NewsletterController@ store method handler
     */
    public function testDestroyRequest()
    {
        /** Test newsletter active state equals true */
        $this->get('newsletter/' . $this->newsletter->email)
            ->seeJson([ 'active' => '1' ]);

        /** Delete newsletter -> active state goes false */
        $this->json('DELETE', '/newsletter/' . $this->newsletter->email)
            ->seeJson(
                [
                    'error' => false,
                ]);

        /** Test newsletter active state equals false */
        $this->get('newsletter/' . $this->newsletter->email)
            ->seeJson([ 'active' => '0' ]);

    }

    /**
     * Test NewsletterController@ index method handler
     */
    public function testIndexRequest(){
        // Test index with not existent email. Must return json with 'newsletter_subscription' => null
        $this->get('newsletter/mail@mail.com')
            ->seeJson([ 'newsletter_subscription' => null]);

        // Add new newsletter subscription. Must return 'error' => false
        $this->json('POST', '/newsletter/store', ['email' => 'testingemail@mail.com'])
            ->seeJson(['error' => false]);

        // Test with just added newsletter subscription. Must return with 'newsletter_subscription' => {newsletter_subscription}
        $this->get('newsletter/testingemail@mail.com')
            ->seeJsonStructure([ 'newsletter_subscription' => ['active', 'created_at']]);
    }
}
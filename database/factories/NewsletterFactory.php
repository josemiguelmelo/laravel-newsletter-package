<?php

$factory->define(\JoseMiguelMelo\Newsletter\Models\Newsletter::class, function (Faker\Generator $faker) {
    return [
        'email'      => $faker->unique()->safeEmail,
        'active'     => true,
        'last_sent'  => \Carbon\Carbon::now(),
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now(),
    ];
});

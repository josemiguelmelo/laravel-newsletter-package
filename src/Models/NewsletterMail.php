<?php

namespace JoseMiguelMelo\Newsletter\Models;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewsletterMail extends Mailable
{
    use Queueable, SerializesModels;

    private $newsletterBody;
    private $fromEmail;
    private $fromName;
    private $emailSubject;

    public function __construct($body, $fromEmail = null, $fromName = null, $emailSubject = null)
    {
        $this->newsletterBody = $body;
        $this->fromEmail = $fromEmail;
        $this->fromName = $fromName;
        $this->emailSubject = $emailSubject;
    }

    public function build()
    {
        $address = ($this->fromEmail == null ? config('newsletters.from_email') : $this->fromEmail);
        $name = ($this->fromName == null ? config('newsletters.from_name') : $this->fromName);
        $emailSubject = ($this->emailSubject == null ? config('newsletters.subject') : $this->emailSubject);

        return $this->view('newsletter::mails.newsletter')
            ->with('newsletter', ['body' => $this->newsletterBody])
            ->from($address, $name)
            ->subject($emailSubject);
    }

}

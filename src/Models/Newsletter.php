<?php

namespace JoseMiguelMelo\Newsletter\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    protected $table = 'newsletters';

    protected $fillable = ['email', 'active', 'last_sent', 'frequency'];

    public static function routes(){
        include __DIR__ . '/../include-routes.php';
    }

    public function getFrequencyAttribute($value)
    {
        switch ($value)
        {
            case 0: return 'daily';
            case 1: return 'weekly';
            case 2: return 'monthly';
            default: return 'daily';
        }
    }
    public function setFrequencyAttribute($value)
    {
        switch ($value)
        {
            case 'daily':
                $this->attributes['frequency'] = 0;
                break;
            case 'weekly':
                $this->attributes['frequency'] = 1;
                break;
            case 'monthly':
                $this->attributes['frequency'] = 2;
                break;
            default:
                $this->attributes['frequency'] = 0;
                break;
        }
    }


    public function mustSend()
    {
        $lastSent = new Carbon($this->last_sent);
        $now = Carbon::now();
        $difference = $lastSent->diff($now)->days;


        if ($this->frequency == 'daily' && $difference >= 1)
            return true;

        if ($this->frequency == 'weekly' && $difference >= 7)
            return true;

        if ($this->frequency == 'monthly' && $difference >= 31)
            return true;

        return false;
    }
}

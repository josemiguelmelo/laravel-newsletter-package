<?php

/** These routes are the ones used when routes are required by the env variable */

Route::get('newsletter/generate',
           ['as' => 'newsletter.generate', 'uses' => 'JoseMiguelMelo\Newsletter\Controllers\NewsletterController@generate']);

Route::post('newsletter/store',
            ['as' => 'newsletter.store', 'uses' => 'JoseMiguelMelo\Newsletter\Controllers\NewsletterController@store']);

Route::post('newsletter/send_all',
            ['as' => 'newsletter.send_all', 'uses' => 'JoseMiguelMelo\Newsletter\Controllers\NewsletterController@sendToAll']);

Route::get('newsletter/{email}',
           ['as' => 'newsletter.index', 'uses' => 'JoseMiguelMelo\Newsletter\Controllers\NewsletterController@index']);

Route::delete('newsletter/{email}',
              ['as' => 'newsletter.destroy', 'uses' => 'JoseMiguelMelo\Newsletter\Controllers\NewsletterController@destroy']);
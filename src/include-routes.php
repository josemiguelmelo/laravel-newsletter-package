<?php

/** These routes are the ones used on Newsletter::routes() */

Route::get('newsletter/generate',
           ['as' => 'newsletter.generate', 'uses' => 'NewsletterController@generate']);

Route::post('newsletter/store',
            ['as' => 'newsletter.store', 'uses' => 'NewsletterController@store']);

Route::post('newsletter/send_all',
            ['as' => 'newsletter.send_all', 'uses' => 'NewsletterController@sendToAll']);

Route::get('newsletter/{email}',
           ['as' => 'newsletter.index', 'uses' => 'NewsletterController@index']);

Route::delete('newsletter/{email}',
              ['as' => 'newsletter.destroy', 'uses' => 'NewsletterController@destroy']);
<?php
namespace JoseMiguelMelo\Newsletter\Commands;


use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use JoseMiguelMelo\Newsletter\Models\Newsletter;
use JoseMiguelMelo\Newsletter\Models\NewsletterMail;

class SendNewsletterCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'newsletter:send {--all_subscribers} {--filters=} {--view=} {--from_email=} {--from_name=} {--subject=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send newsletter to all subscribers.';

    private $list;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info('Send Newsletter Command');

        $subscribers = [];

        $this->handleFiltersOption($subscribers);

        foreach ($subscribers as $subscriber) {
            if ($subscriber->mustSend() || $this->option('all_subscribers') == true) {
                Log::info('Newsletter sent to ' . $subscriber->email);

                $this->sendEmailTo($subscriber->email);

                $this->updateSubscriberLastSentMessage($subscriber);
            }
        }
    }

    private function handleFiltersOption(&$subscribers)
    {
        if ( ! is_null($this->option('filters'))) {
            $filters = json_decode($this->option('filters'));

            if ( ! is_array($filters[0]))
                $filters = [$filters];

            $subscribers = Newsletter::where('active', '=', true)
                ->where($filters)
                ->get();
        } else {
            $subscribers = Newsletter::where('active', '=', true)
                ->get();
        }
    }

    private function handleNewsletterFromInformation(&$fromEmail, &$fromName, &$subject)
    {
        $fromEmail = null; $fromName = null; $subject = null;

        if ( ! is_null($this->option('from_email'))) {
            $fromEmail = $this->option('from_email');
        }
        if ( ! is_null($this->option('from_name'))) {
            $fromName = $this->option('from_name');
        }
        if ( ! is_null($this->option('subject'))) {
            $subject = $this->option('subject');
        }
    }

    private function sendEmailTo($email)
    {
        $view = $this->handleViewOption();

        $fromEmail = null; $fromName = null; $subject = null;
        $this->handleNewsletterFromInformation($fromEmail, $fromName, $subject);

        Mail::to($email)->queue(new NewsletterMail($view, $fromEmail, $fromName, $subject));
    }

    private function handleViewOption(){
        if ( ! is_null($this->option('view'))) {
            if ($this->isHtml($this->option('view'))) {
                return $this->option('view');
            } elseif (filter_var($this->option('view'), FILTER_VALIDATE_URL)) {
                $webpage = file_get_contents($this->option('view'));

                if ( ! $this->is_utf8($webpage))
                    $webpage = utf8_decode($webpage);
                return preg_replace('#<script(.*?)>(.*?)</script>#is', '', $webpage);
            } // If it is not html nor url, send default newsletter view
            else {
                return view(config('newsletters.newsletter_view'))->render();
            }
        }
        // If view option not set, send default newsletter view
        return view(config('newsletters.newsletter_view'))->render();
    }

    private function is_utf8($string)
    {
        return (mb_detect_encoding($string, 'UTF-8', true) == 'UTF-8');
    }
    
    private function isHtml($string)
    {
        return preg_match("/<[^<]+>/", $string, $m) != 0;
    }

    private function updateSubscriberLastSentMessage($subscriber)
    {
        $subscriber->last_sent = Carbon::now();
        $subscriber->save();
    }
}

<?php
namespace JoseMiguelMelo\Newsletter\Commands;

use Illuminate\Console\Command;
use Illuminate\Container\Container;
use Illuminate\Support\Facades\Log;
use League\Flysystem\Exception;

class NewsletterMigrateCommand extends Command
{
    /**
     * Name of the command.
     *
     * @param string
     */
    protected $signature = 'newsletter:migrate';

    /**
     * Necessary to let people know, in case the name wasn't clear enough.
     *
     * @param string
     */
    protected $description = 'Migrate newsletter package migration files.';

    /**
     * Setup the application container as we'll need this for running migrations.
     */
    public function __construct(Container $app)
    {
        parent::__construct();
        $this->app = $app;
    }

    /**
     * Run the package migrations.
     */
    public function handle()
    {
        $migrations = $this->app->make('migration.repository');
        try {
            $migrations->createRepository();
        }catch (Exception $exception)
        {
            Log::info("Migration repos already created");
        }

        $migrator = $this->app->make('migrator');
        $migrator->run(__DIR__.'/../../database/migrations');
        $migrator->run(__DIR__.'/Fixtures/Migrations');
    }
}

<?php
namespace JoseMiguelMelo\Newsletter\Commands;

use Illuminate\Console\Command;
use Illuminate\Container\Container;

class NewsletterRollbackCommand extends Command
{
    /**
     * Name of the command.
     *
     * @param string
     */
    protected $signature = 'newsletter:rollback';

    /**
     * Necessary to let people know, in case the name wasn't clear enough.
     *
     * @param string
     */
    protected $description = 'Rollback newsletter package migration files.';

    /**
     * Setup the application container as we'll need this for running migrations.
     */
    public function __construct(Container $app)
    {
        parent::__construct();
        $this->app = $app;
    }

    /**
     * Run the package migrations.
     */
    public function handle()
    {
        $migrator = $this->app->make('migrator');
        $migrator->rollback(__DIR__.'/../../database/migrations');
    }
}

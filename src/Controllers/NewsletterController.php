<?php
namespace JoseMiguelMelo\Newsletter\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use JoseMiguelMelo\Newsletter\Models\Newsletter;

class NewsletterController extends Controller
{
    public function index($email)
    {
        $newsletterSubscription = Newsletter::whereEmail($email)->first();

        return response()->json(
            [
                'newsletter_subscription' => $newsletterSubscription
            ]);
    }

    public function store(Request $request)
    {
        $input = $request->all();

        if ( ! isset($input['email']))
            return response()->json(
                [
                    'error' => true,
                    'msg'   => 'Email required',
                ]
            );

        try {
            $newsletter = Newsletter::create(['email' => $input['email'], 'active' => true, 'last_sent' => \Carbon\Carbon::now()]);
        } catch (QueryException $exception) {
            return response()->json(['error' => true, 'msg' => 'An exception was thrown while creating the newsletter subscription.']);
        }

        return response()->json(
            [
                'error'                   => false,
                'newsletter_subscription' => $newsletter,
            ]);
    }

    public function sendToAll(Request $request)
    {
        try {
            Artisan::call('newsletter:send', ['--all_subscribers' => true]);
        }catch (\Exception $exception)
        {
            return response()->json(['error' => true, 'msg' => 'Something went wrong while running command.', 'exception' => $exception->getMessage()]);
        }
        return response()->json(['error' => false, 'msg' => 'Emails are being sent.']);
    }

    public function destroy($email)
    {
        try{
            Newsletter::where('email', $email)->update(['active' => false]);
        }catch (\Exception $exception)
        {
            return response()->json(['error' => true, 'msg' => 'An exception was thrown while destroying the newsletter subscription..', 'exception' => $exception->getMessage()]);
        }
        return response()->json(['error' => false, 'msg' => 'Newsletter destroyed successfully.']);
    }

    public function generate()
    {
        $newsletter = factory(Newsletter::class)->make();
        $newsletter->save();
        return response()->json($newsletter);
    }
}

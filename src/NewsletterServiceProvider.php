<?php
namespace JoseMiguelMelo\Newsletter;

use Illuminate\Support\ServiceProvider;
use Faker\Generator as FakerGenerator;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;


class NewsletterServiceProvider extends ServiceProvider
{

    protected $commands = [
        'JoseMiguelMelo\Newsletter\Commands\NewsletterMigrateCommand',
        'JoseMiguelMelo\Newsletter\Commands\NewsletterRollbackCommand',
        'JoseMiguelMelo\Newsletter\Commands\SendNewsletterCommand',
    ];

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'newsletter');

        $this->publishElements();

        if(config('newsletters.include_routes')) {
            include __DIR__ . '/routes.php';

            $this->app->make('JoseMiguelMelo\Newsletter\Controllers\NewsletterController');
        }
    }

    private function publishElements()
    {
        $configPath = __DIR__ . '/../config/newsletters.php';
        $this->publishes([$configPath => config_path('newsletters.php')]);

        $viewsPath = __DIR__ . "/../resources/views/mails";
        $this->publishes([$viewsPath => base_path("resources/views/vendor/newsletter/mails")], 'mails');

        $migrationsPath = __DIR__ . "/../database/migrations";
        $this->publishes([$migrationsPath => base_path("database/migrations")], 'migrations');

        $controllersPath = __DIR__ . "/Controllers";
        $this->publishes([$controllersPath => base_path("app/Http/Controllers")], 'controllers');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(EloquentFactory::class, function ($app) {
            $faker = $app->make(FakerGenerator::class);
            $factories_path = __DIR__ . '/../database/factories';
            return EloquentFactory::construct($faker, $factories_path);
        });

        if(config('newsletters.cronjob')) {
            $this->app->singleton('JoseMiguelMelo.newsletter.console.kernel', function ($app) {
                $dispatcher = $app->make(\Illuminate\Contracts\Events\Dispatcher::class);
                return new \JoseMiguelMelo\Newsletter\Console\Kernel($app, $dispatcher);
            });

            $this->app->make('JoseMiguelMelo.newsletter.console.kernel');
        }

        $this->commands($this->commands);
    }
}

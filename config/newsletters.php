<?php

return [
    'from_email'      => env('NEWSLETTER_EMAIL', 'newsletters@email.com'),
    'from_name'       => env('NEWSLETTER_NAME', 'Newsletters'),
    'subject'         => env('NEWSLETTER_SUBJECT', 'Newsletters subject'),
    'newsletter_view' => env('NEWSLETTER_VIEW', 'vendor.newsletter.mails.default'),
    'include_routes'  => env('NEWSLETTER_ROUTES', false),
    'cronjob'         => env('NEWSLETTER_CRONJOB', true),
];